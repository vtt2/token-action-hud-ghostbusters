import {
    TALENTS_ID, DEFAULTS,
} from './defaults.js';
export let ActionHandler = null

Hooks.once('tokenActionHudCoreApiReady', async (coreModule) => {
    ActionHandler = class ActionHandler extends coreModule.api.ActionHandler {
        async buildSystemActions() {
            const token = this?.token;
            if (!token) return;
            const actor = this?.actor;
            if (!actor) return;

            for (let t in actor.system.traits) {
                if (this.actor.system.traits[t].active && t !== 'ecto_presence' && this.actor.system.traits.current !== 0) {
                    const TRAIT_ID = t;
                    const GROUP_ID = TRAIT_ID + "_" + TALENTS_ID;
                    const TALENT_ID = GROUP_ID + "_" + "talent";

                    const traitEncodedValue = [game.i18n.localize(this.actor.system.traits[t].label), this.actor.system.traits[t].current].join(this.delimiter);
                    await this.addActions([{
                        name: game.i18n.localize(this.actor.system.traits[t].label),
                        id: t,
                        encodedValue: traitEncodedValue
                    }], {id: TRAIT_ID, type: 'system'});

                    if (t !== 'power') {
                        const talentName = this.actor.system.traits[t].attached.names;
                        const talentEncodedValue = [talentName, this.actor.system.traits[t].attached.value].join(this.delimiter);
                        await this.addActions([{
                            name: talentName,
                            id: TALENT_ID,
                            encodedValue: talentEncodedValue
                        }], {id: GROUP_ID, type: 'system'});
                    }
                }
            }
        }
    }
})
