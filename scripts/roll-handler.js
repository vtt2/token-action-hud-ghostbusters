export let RollHandler = null

Hooks.once('tokenActionHudCoreApiReady', async (coreModule) => {
  RollHandler = class RollHandler extends coreModule.api.RollHandler {

    /** @override */
    async handleActionClick(event, encodedValue) {
      let payload = encodedValue.split(this.delimiter);

      if (payload.length !== 2) {
        super.throwInvalidValueErr();
      }

      if (payload[1] === 0) {
        ui.notifications.warn(game.i18n.localize("GB.SCORE_0"));
        return;
      }

      const rollData = {};
      rollData.roll = payload[1];
      rollData.label = payload[0];
      rollData.brownie_points = 0;
      rollData.bonus = 0;
      rollData.penalty = 0;
      rollData.actor = this.actor;
      rollData.bpcostcolor = 'black';
      game.Gb.GbRoll.rollDialog(rollData);
    }
  }
})
