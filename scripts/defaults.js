/**
 * Default categories and subcategories
 */

export const TRAITS_ID = 'traits';
export const TALENTS_ID = 'talents';

export const BRAINS_ID = 'brains';

export const MUSCLES_ID = 'muscles';

export const MOVES_ID = 'moves';

export const COOL_ID = 'cool';

export const POWER_ID = 'power';

export let DEFAULTS = null;

Hooks.on('i18nInit', async () => {
    const TRAITS_NAME = game.i18n.localize('GB.TRAITS');
    const TALENTS_NAME = game.i18n.localize('GB.TALENTS');
    const BRAINS_NAME = game.i18n.localize('GB.BRAINS');
    const MUSCLES_NAME = game.i18n.localize('GB.MUSCLES');
    const MOVES_NAME = game.i18n.localize('GB.MOVES');
    const COOL_NAME = game.i18n.localize('GB.COOL');
    const POWER_NAME = game.i18n.localize('GB.POWER')

    DEFAULTS = {
        layout: [
            {
                nestId: BRAINS_ID,
                id: BRAINS_ID,
                name: BRAINS_NAME,
                type: 'system',
                groups: [
                    {
                        nestId: BRAINS_ID + "_" + BRAINS_ID,
                        id: BRAINS_ID,
                        name: BRAINS_NAME,
                        type: 'system',
                        groups: [
                            {
                                nestId: BRAINS_ID + "_" + BRAINS_ID + "_" + TALENTS_ID,
                                id: BRAINS_ID + '_' + TALENTS_ID,
                                name: TALENTS_NAME,
                                type: 'system',
                            },
                        ]
                    },
                ],
            },
            {
                nestId: MUSCLES_ID,
                id: MUSCLES_ID,
                name: MUSCLES_NAME,
                type: 'system',
                groups: [
                    {
                        nestId: MUSCLES_ID + "_" + MUSCLES_ID,
                        id: MUSCLES_ID,
                        name: MUSCLES_NAME,
                        type: 'system',
                        groups: [
                            {
                                nestId: MUSCLES_ID + "_" + MUSCLES_ID + "_" + TALENTS_ID,
                                id: MUSCLES_ID + '_' + TALENTS_ID,
                                name: TALENTS_NAME,
                                type: 'system',
                            },
                        ]
                    },
                ],
            },
            {
                nestId: MOVES_ID,
                id: MOVES_ID,
                name: MOVES_NAME,
                type: 'system',
                groups: [
                    {
                        nestId: MOVES_ID + "_" + MOVES_ID,
                        id: MOVES_ID,
                        name: MOVES_NAME,
                        type: 'system',
                        groups: [
                            {
                                nestId: MOVES_ID + "_" + MOVES_ID + "_" + TALENTS_ID,
                                id: MOVES_ID + '_' + TALENTS_ID,
                                name: TALENTS_NAME,
                                type: 'system',
                            },
                        ]
                    },
                ],
            },
            {
                nestId: COOL_ID,
                id: COOL_ID,
                name: COOL_NAME,
                type: 'system',
                groups: [
                    {
                        nestId: COOL_ID + "_" + COOL_ID,
                        id: COOL_ID,
                        name: COOL_NAME,
                        type: 'system',
                        groups: [
                            {
                                nestId: COOL_ID + "_" + COOL_ID + "_" + TALENTS_ID,
                                id: COOL_ID + '_' + TALENTS_ID,
                                name: TALENTS_NAME,
                                type: 'system',
                            },
                        ]
                    },
                ],
            },
            {
                nestId: POWER_ID,
                id: POWER_ID,
                name: POWER_NAME,
                type: 'system',
                groups: [
                    {
                        nestId: POWER_ID + "_" + POWER_ID,
                        id: POWER_ID,
                        name: POWER_NAME,
                        type: 'system'
                    },
                ],
            },
        ],
        group: [
            {id: BRAINS_ID, name: BRAINS_NAME, type: 'system', hasDerivedSubcategories: true},
            {id: MUSCLES_ID, name: MUSCLES_NAME, type: 'system', hasDerivedSubcategories: true},
            {id: MOVES_ID, name: MOVES_NAME, type: 'system', hasDerivedSubcategories: true},
            {id: COOL_ID, name: COOL_NAME, type: 'system', hasDerivedSubcategories: true},
            {id: POWER_ID, name: POWER_NAME, type: 'system', hasDerivedSubcategories: false},
        ]
    }
})
